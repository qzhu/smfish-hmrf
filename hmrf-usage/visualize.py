#!/usr/bin/python
import numpy as np
import math
import pandas as pd
import seaborn as sns
import sys
import scipy
import scipy.stats
import matplotlib.pyplot as plt
sys.path.insert(0, "../svmPredict/lib")
import reader
sys.setrecursionlimit(10000)
#sns.set_style("white")
#boi is beta of interest
def plot_all_fd_all_gene_stitched_9(directory, roi, Xcen, FDs, boi, k, field, 
reversey=False, dot_size = 45, title=True, lim=5000):
	print "ALL FD", "k is", k, "boi is", boi, directory
	colors = {}
	colors[1] = (0, 107, 12) #dark green
	colors[2] = (0, 248, 134) #green
	colors[3] = (96, 210, 255) #light blue
	colors[4] = (0, 111, 255) #blue
	colors[7] = (0, 0, 160) #dark blue
	colors[6] = (217, 7, 9) #dark red
	colors[5] = (124, 0, 124) #purple
	colors[8] = (255, 226, 6) #yellow
	colors[9] = (207, 140, 4) #beige
	for c in colors:
		a1, a2, a3 = colors[c]
		colors[c] = (a1/255.0, a2/255.0, a3/255.0)
	cls = {}
	if boi=="kmeans":
		cls[boi] = reader.read_kmeans("%s/k_%d/f%s.gene.ALL.kmeans.txt" % (directory, k, roi))
	else:
		cls[boi] = reader.read_classes("%s/k_%d/f%s.gene.ALL.beta.%s.prob.txt" % (directory, k, roi, boi), plot=False)
	Xcen2 = np.empty(Xcen.shape, dtype="float32")
	for i in range(Xcen.shape[0]):
		Xcen2[i,0] = Xcen[i,0]
		Xcen2[i,1] = Xcen[i,1]
		if reversey:
			Xcen2[i,1] = -1.0 * Xcen[i,1]
	ncol = 1
	nrow = 1
	size_factor = 10 #previous 2
	dot_size = dot_size
	#size_factor = 5
	#dot_size = 15
	#if len(FDs)%ncol>0:
	#	nrow+=1
	#sns.set_style("darkgrid")
	#sns.set_style("white", {"figure.facecolor": "black"})
	#sns.axes_style("white")
	f, axn = plt.subplots(nrow, ncol, figsize=(ncol * size_factor, nrow * size_factor), sharex=True, sharey=True)
	plt.subplots_adjust(hspace=0.01, wspace=0.01)
	ct = 0
	fx = 100
	m1 = np.where((cls[boi]>0) & (field==fx))
	cm = plt.cm.get_cmap("RdYlBu_r")
	axn.set_axis_bgcolor("black")
	#axn.scatter(Xcen2[m1][:,0], Xcen2[m1][:,1], s=dot_size, c=cls[boi][m1], edgecolors="gray", cmap=cm, vmin=1, vmax=max(cls[boi]))
	for c in colors:
		m2 = np.where((cls[boi]==c) & (field==fx))
		axn.scatter(Xcen2[m2][:,0], Xcen2[m2][:,1], s=dot_size, color=colors[c], edgecolors="gray")
	axn.set_xlim(0, lim)
	axn.set_ylim(-1 * lim, 0)
	
def plot_all_fd_all_gene_stitched(directory, roi, Xcen, FDs, boi, k, field, 
reversey=False, dot_size = 45, title=True, lim=5000):
	print "ALL FD", "k is", k, "boi is", boi, directory
	cls = {}
	if boi=="kmeans":
		cls[boi] = reader.read_kmeans("%s/k_%d/f%s.gene.ALL.kmeans.txt" % (directory, k, roi))
	else:
		cls[boi] = reader.read_classes("%s/k_%d/f%s.gene.ALL.beta.%s.prob.txt" % (directory, k, roi, boi), plot=False)
	Xcen2 = np.empty(Xcen.shape, dtype="float32")
	for i in range(Xcen.shape[0]):
		Xcen2[i,0] = Xcen[i,0]
		Xcen2[i,1] = Xcen[i,1]
		if reversey:
			Xcen2[i,1] = -1.0 * Xcen[i,1]
	ncol = 1
	nrow = 1
	size_factor = 10 #previous 2
	dot_size = dot_size
	f, axn = plt.subplots(nrow, ncol, figsize=(ncol * size_factor, nrow * size_factor), sharex=True, sharey=True)
	plt.subplots_adjust(hspace=0.01, wspace=0.01)
	ct = 0
	fx = 100
	m1 = np.where((cls[boi]>0) & (field==fx))
	cm = plt.cm.get_cmap("RdYlBu_r")
	axn.scatter(Xcen2[m1][:,0], Xcen2[m1][:,1], s=dot_size, c=cls[boi][m1], edgecolors="gray", cmap=cm, vmin=1, vmax=max(cls[boi]))
	axn.set_xlim(0, lim)
	axn.set_ylim(-1 * lim, 0)
	
def get_cluster_expression(directory, roi, boi, Xcen, FDs, genes, k, field):
	expr = {}
	for g in genes:
		expr[g]= reader.read_expression("%s/data/f%s.gene.%s.txt" % (directory, roi, g))
	cls = {}
	if boi=="kmeans":
		cls[boi] = reader.read_kmeans("%s/k_%d/f%s.gene.ALL.kmeans.txt" % (directory, k, roi))
	else:
		cls[boi] = reader.read_classes("%s/k_%d/f%s.gene.ALL.beta.%s.prob.txt" % (directory, k, roi, boi), plot=False)
	nj_union = []
	for ki in range(1, k+1):
		#if ki==1 or ki==15: continue
		m = np.where(cls[boi]==ki)[0]
		nx = []
		for g in genes:
			es = expr[g][m]
			nx.append((ki, g, np.mean(es), np.std(es)))
		nx.sort(lambda x,y:cmp(x[2], y[2]), reverse=True)
		for ni, nj, nk, nl in nx[:10]:
			nj_union = nj_union + [nj]
		for ni, nj, nk, nl in nx[-10:]:
			nj_union = nj_union + [nj]
	
	nj_union = set(nj_union)
	nj_union = genes
	#nj_union = list(set(nj_union))
	cluster_expr = np.empty((len(nj_union), k), dtype="float32")
	nj_union_title = []
	for n in nj_union:
		nj_union_title.append(n.title())
	nt = {}
	for ki in range(1, k+1):
		#if ki==1 or ki==15: continue
		m = np.where(cls[boi]==ki)[0]
		nx = []
		for g in nj_union:
			es = expr[g][m]
			nx.append((ki, g, np.mean(es), np.std(es)))
		cluster_expr[:, ki-1] = np.array([n[2] for n in nx]) 
		nt[ki] = pd.Series([tx[2] for tx in nx], index=nj_union_title)
		
	#print pd.DataFrame(nt)
	#f2,axn = plt.subplots(1,1, figsize=(9,20))
	#ax = sns.heatmap(pd.DataFrame(nt), annot=False, fmt=".2f", ax=axn)
	#sns.set(font_scale=1.0)
	cg=sns.clustermap(pd.DataFrame(nt), row_cluster=True, col_cluster=True, \
	figsize=(5, 20), method="average", vmax=1.5, vmin=-1.5, col_ratios={"dendrogram":0.05}, \
	row_ratios={"dendrogram":0.05})
	plt.setp(cg.ax_heatmap.yaxis.get_majorticklabels(), rotation=0)

	
if __name__=="__main__":
	#=========================================================
	k_param = 9 #INPUT <-
	boi = "9.0" #beta: 0.0, 1.0, 5.0, 9.0, or kmeans #INPUT <-
	#boi = "kmeans"
	#=========================================================
	sns.set_style("dark")
	
	directory = "."
	cortex_FDs = [100]
	FDs = cortex_FDs
	gene = "ALL"

	roi = "cortex"
	genes = reader.read_genes("HMRF.genes")
	Xcen, field = reader.read_coord("f%s.coordinates.txt" % roi)

	#special dark-background visualization for K=9
	plot_all_fd_all_gene_stitched_9(directory, roi, Xcen, FDs, boi, k_param, field, \
	reversey=False, dot_size=45, title=True, lim=6100)
	
	#please use this for all other K's, light-grey background
	#plot_all_fd_all_gene_stitched(directory, roi, Xcen, FDs, boi, k_param, field, \
	#reversey=False, dot_size=45, title=True, lim=6100)
	
	get_cluster_expression(directory, roi, boi, Xcen, cortex_FDs, genes, k_param, \
	field)
	plt.show()
	
