do_one ()
{
xi=$1 #file name
xj=$2 #prefix
xg=$3 #gene name
xbeta=$4
xk=$5
./getHMRFEM.multi.beta.R $xi $xj.adjacency.txt $xj.blocks.txt $xj.gene.$xg.centroid.txt $xbeta $xk $xj.gene.$xg.beta.$xbeta.prob.txt $xj.gene.$xg.beta.$xbeta.hmrf.centroid.txt $xj.gene.$xg.beta.$xbeta.hmrf.covariance.txt

mv *.ALL.beta.* "k_$xk"/.
cp *.ALL.kmeans.txt *.ALL.centroid.txt "k_$xk"/.
}

if [ ! -f "roi" ]; then
echo "ROI file does not exist. Assume this is cortex."
roi="cortex"
else
roi=`cat roi`
fi

i="f$roi.gene.ALL.txt"

j=`echo $i|cut -d "." -f1`
g=`echo $i|cut -d "." -f3`

#for k in `seq 2 1 15`; do
k=9
if [ ! -d "k_$k" ]; then
mkdir "k_$k"
fi
echo "Setting: K=$k"
echo "Finding initial centroids..."
./generateCentroids.multi.R $k $i $j.gene.$g.centroid.txt $j.gene.$g.kmeans.txt
echo "Done."
cp $j.gene.$g.centroid.txt $j.gene.$g.kmeans.txt "k_$k"/.
for beta in `seq 0 1.0 9.0`; do
echo "Setting: beta=$beta"
echo "Begin HMRF..."
do_one $i $j $g $beta $k
echo "Done."
done
#done
