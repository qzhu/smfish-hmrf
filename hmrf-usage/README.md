# Usage examples for spatial pattern mining using Hidden Markov Random Field (HMRF)

Requires smfishHmrf R package to be installed (see [../smfishHmrf/INSTALL.md](../smfishHmrf/INSTALL.md))

## Instructions:

1. `./script_beta.sh`

    This will generate initial centroids to begin HMRF (generateCentroids.multi.R). 
    Then it will call HMRF R package (in script getHMRFEM.multi.beta.R) to begin 
    mining given the initial conditions, the parameters K, beta, and input datasets 
    expression matrix, adjacency list (for neighbor graph). 

    By default, this script will run for K=9, and run through multiple beta's, and 
    save output in the directory k_9. The script can be easily customized to loop
    through multiple K's. See script for details.

2. `./visualize.py`

    This will display a spatial map of cortex colored according to HMRF domains.
    Change the parameters "k_param", and "boi" (beta of interest) in the __main__ 
    method of this script to display the HMRF results for a given K and beta. 
    As it is, it displays K=9, boi=9.0.


## Input files:

1. Expression matrix:

    `fcortex.gene.ALL.txt`

    Row (cell), and column (gene) z-scored expression. 
    First column is cell ID. Order of genes is specified in file HMRF.genes.

2. Adjacency list:

    `fcortex.adjacency.txt`

    Each row is a cell and its neighbors.

3. Initial conditions:

    `fcortex.gene.ALL.centroid.txt`
 
    initial centroid positions

4. Node independence (for updating in EM):

    `fcortex.blocks.txt`

    During HMRF EM inference step, this file specifies the blocks of nodes such that 
    the nodes within each block are independent given the other blocks. Nodes within 
    each block are updated together during EM inference. In this file, each line 
    (a node) details the block to which it belongs. We generated this file by the 
    GraphColoring Java pacakge, since the partitioning of nodes to blocks is 
    related to the graph coloring problem.

5. Coordinates:

    `fcortex.coordinates.txt`

    Coordinates of the cells (for visualization).


## Examples

See directory [examples](examples)

See running logs [logs](logs/log.jan3)
