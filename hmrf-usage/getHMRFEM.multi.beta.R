#!/usr/bin/Rscript

mem_file <- commandArgs(trailingOnly = TRUE)[1]
nei_file <- commandArgs(trailingOnly = TRUE)[2]
block_file <- commandArgs(trailingOnly = TRUE)[3]
centroid_file <- commandArgs(trailingOnly = TRUE)[4]
beta <- commandArgs(trailingOnly = TRUE)[5]
par_k <- commandArgs(trailingOnly = TRUE)[6]
out_file <- commandArgs(trailingOnly = TRUE)[7] #hmrfem probability
out_file_2 <- commandArgs(trailingOnly = TRUE)[8] #hmrfem centroids
out_file_3 <- commandArgs(trailingOnly = TRUE)[9] #hmrfem covariance

out_file_unnorm <- gsub("prob", "unnormprob", out_file)

beta<-as.double(beta)
par_k <- as.integer(par_k)

library(dplyr)
library(smfishHmrf)
y<-read.table(mem_file, header=F, row.names=1)
y<-as.matrix(y)
nei<-read.table(nei_file, header=F, row.names=1)
colnames(nei)<-NULL
rownames(nei)<-NULL
nei<-as.matrix(nei)
blocks<-read.table(block_file, header=F, row.names=1)
blocks<-c(t(blocks))
maxblock <- max(blocks)
blocks<-lapply(1:maxblock, function(x) which(blocks == x))
numnei<-apply(nei, 1, function(x) sum(x!=-1))
k<-par_k
m<-dim(y)[2]

centroid<-read.table(centroid_file, header=F, row.names=1)
centroid<-as.matrix(centroid)

sigma <-array(0, c(m,m,k))
for(i in 1:k){
sigma[, ,i] <- cov(y)
}

mu<-array(0, c(m,k))
kk2<-centroid
for(i in 1:k){
mu[,i] <- kk2[i,]
}

numcell<-dim(y)[1]
kk_dist<-array(0, c(numcell, k))
for(i in 1:numcell){
for(j in 1:k){
	kk_dist[i,j] <- dist(rbind(y[i,], mu[,j]), method="euclidean")
}
}
clust_mem<-apply(kk_dist, 1, function(x) which(x==min(x))) 
lclust<-lapply(1:k, function(x) which(clust_mem == x))
for(i in 1:k){
sigma[, , i] <- cov(y[lclust[[i]], ])
}

tc.hmrfem<-smfishHmrf.hmrfem.multi(y=y, neighbors=nei, beta=beta, numnei=numnei, blocks=blocks, mu=mu, sigma=sigma, verbose=T, err=1e-7, maxit=50, addDiag=FALSE)

write.table(tc.hmrfem$prob, file=out_file, sep=" ", quote=F, col.names=F, row.names=T)
write.table(tc.hmrfem$unnormprob, file=out_file_unnorm, sep=" ", quote=F, col.names=F, row.names=T)
write.table(t(tc.hmrfem$mu), file=out_file_2, sep=" ", quote=F, col.names=F, row.names=T)
write.table(tc.hmrfem$sigma[,,1], file=out_file_3, sep=" ", quote=F, col.names=F, row.names=T)
for(i in 2:k){
write.table(tc.hmrfem$sigma[,,i], file=out_file_3, sep=" ", quote=F, col.names=F, row.names=T, append=T)
}
