# smFish spatial pattern mining and cell type prediction

This package is organized into two components:

1. smfishHmrf R package (directory: [smfishHmrf](smfishHmrf))
2. SVM prediction Python script (directory: [svmPredict](svmPredict))

The components are independent (neither needs the other package to run).
The third directory ([hmrf-usage](hmrf-usage)) contains important usage examples to help
users get familiar with using the smfishHmrf R package for spatial pattern 
mining.



## Installing smfishHmrf R package

to build:

`tar -czf smfishHmrf.tar.gz smfishHmrf`

to install:

`R CMD INSTALL smfishHmrf.tar.gz`


## Prerequisites for SVM predictions

`matplotlib`, `seaborn`, `pandas`, `numpy`, `scipy`

If any of the above packages is missing, please install it using pip:

`pip install <package name>`

Or if you would like to install per user:

`pip install --user <package name>`

Note that the Python script is written in Python 2.7 and is incompatible with Python 3.


## Running smfishHmrf

See [hmrf-usage/README.md](hmrf-usage/README.md)


## Running cell type prediction

See [svmPredict/README.md](svmPredict/README.md)


## Source data

See [SOURCE_DATA.md](SOURCE_DATA.md)
