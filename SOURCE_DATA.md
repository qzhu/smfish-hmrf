# Cortex seqFISH data

## Cell coordinates:
`hmrf-usage/data/fcortex.coordinates.txt`

## Cell expression:

`hmrf-usage/data/fcortex.gene.*.txt`    (individual gene's expression) (z-scored)

`hmrf-usage/data/fcortex.gene.ALL.txt`  (matrix incorporating all 69 spatial genes' expression) (z-scored)



# SVM prediction data

## Cross-platform normalized expression:
`svmPredict/data/tasic_training_b2.txt`   (scRNAseq data Tasic et al)

`svmPredict/data/seqfish_cortex_b2_testing.txt` (seqFISH data above)

