# SVM cell type prediction

## Training data:

`data/tasic_training_b2.txt` - Cross-platform quantile normalized expression matrix, scaled to 0 - 100

`data/tasic.labels` - Cell type labels

`data/tasic.genes.MAST` - Differentially expressed genes, overlapped with SeqFISH, used for training & prediction

## Testing data:

`data/seqfish_cortex_b2_testing.txt` - Cross-platform quantile normalized expression matrix

## Instructions:

`./prediction_2.py 1e-6 result.txt`

where:

`1e-6` is the value of C (can be any values as large as 1000 or as small as 1e-7. Recommended: 1e-5, 1e-6)

`result.txt` is the output file

This will also show a figure of the probability distribution for each cell type class.


## Output file specification:

Fields (tab-delimited)

1. Cell ID

2. (irrelevant)

3. Cell type class

4. ID of cell type class

5. (irrelevant)

6. Probability of the highest probability class
