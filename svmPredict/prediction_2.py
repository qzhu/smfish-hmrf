#!/usr/bin/python
import numpy as np
import pandas as pd
import seaborn as sns
import sys
import scipy
import scipy.stats
import matplotlib.pyplot as plt
import matplotlib as mpl
import os
sys.path.insert(0, "lib")
import reader
sys.setrecursionlimit(10000)
from sklearn import svm
from sklearn.calibration import CalibratedClassifierCV
import prediction_main

if __name__=="__main__":
	file1, file2 = "", ""
	file1 = "data/seqfish_cortex_b2_testing.txt"
	file2 = "data/seqfish_cortex_b2.labels" #not used
	train1 = "data/tasic_training_b2.txt"
	train2 = "data/tasic.labels"
	
	X, genes, Y, Y_2, Y_id = prediction_main.read_tasic_X(\
	train1, train2)
	
	np_Y = np.array(Y)
	use_genes = reader.read_genes("data/tasic.genes.MAST")
	X_copy_1 = np.copy(X)
	X_copy_1 = X_copy_1[:,prediction_main.get_ID(use_genes, genes)]

	test_X, test_genes, test_Y, test_Y_2 = prediction_main.read_seqfish_X(\
	file1, file2) #file2, test_Y, test_Y_2 are not used
	
	C = float(sys.argv[1]) #input parameter
	outfile = sys.argv[2]  #output file

	debug = 0
	clf = svm.LinearSVC(class_weight="balanced", dual=False, C=C, \
	verbose=debug, max_iter=10000, tol=1e-4)
	
	clf.fit(X_copy_1, np_Y)
	pr = clf.predict(test_X[:,prediction_main.get_ID(use_genes,test_genes)])
	dec = clf.decision_function(test_X[:,prediction_main.get_ID(use_genes, test_genes)])

	clf_sigmoid = CalibratedClassifierCV(clf, cv="prefit")
	clf_sigmoid.fit(test_X[:,prediction_main.get_ID(use_genes,test_genes)], pr)

	pr = clf_sigmoid.predict(test_X[:,prediction_main.get_ID(use_genes,test_genes)])
	prob_pr = clf_sigmoid.predict_proba(test_X[:,prediction_main.get_ID(use_genes,test_genes)])

	background = []
	sc = []
	for il in range(len(test_Y)):
		aSc = []
		for dx in range(dec[il].shape[0]):
			background.append(prob_pr[il][dx])
			aSc.append(prob_pr[il][dx])
		sc.append(aSc)
	
	fw = open(outfile, "w")
	cell_map = {}
	cell_map_rev = {}
	for il, l in enumerate(test_Y):
		dd = dec[il]
		dd_ind = np.argmax(prob_pr[il])
		dd_min = np.max(dd)
		this_prob = prob_pr[il, dd_ind]
		cell_map[pr[il]] = dd_ind
		cell_map_rev[dd_ind] = pr[il]
		#test_Y_2 is cell ID
		fw.write("%s\t%s\t%s\t%d\t-\t%.7f\n" % (test_Y_2[il], l, pr[il], dd_ind, this_prob))
	fw.close()

	#sc is probability matrix (number of cells x number of cell types)
	sc_mat = np.empty((len(test_Y), len(sc[0])), dtype="float32")
	print sc_mat.shape
	for il in range(len(test_Y)):
		for ib in range(len(sc[il])):
			sc_mat[il, ib] = sc[il][ib]
	
	print cell_map
	nrow = 2
	ncol = 5
	size_factor = 3
	f, axn = plt.subplots(nrow, ncol, figsize=(ncol * size_factor, nrow * size_factor), \
	sharex=False, sharey=False)
	plt.subplots_adjust(hspace=0.1, wspace=0.1)
	for i in range(8):
		axn.flat[i].text(0.1, 0.95, "%s" % cell_map_rev.get(i, "Und"), horizontalalignment='center', \
        verticalalignment='center', transform=axn.flat[i].transAxes)
		axn.flat[i].hist(sc_mat[:, i], bins=20)
		if "Neuron" not in cell_map_rev.get(i, "Und"):
			axn.flat[i].set_ylim(0, 200)
	plt.show()	

