#!/usr/bin/python

import numpy as np
import math
import pandas as pd
import seaborn as sns
import sys
import scipy

def row_col_normalize(N, row=True, col=True):
	num_col = N.shape[1]
	num_row = N.shape[0]
	if row:
		for g in range(0, num_row):
			this_mean = np.average(N[g, :])
			this_std = np.std(N[g, :])
			N[g, :] = (N[g, :] - this_mean) / this_std
	if col:
		for c in range(0, num_col):
			this_mean = np.average(N[:, c])
			this_std = np.std(N[:, c])
			N[:, c] = (N[:, c] - this_mean) / this_std
	return N

def read_seqfish_X(expressionFile, labelFile):
	genes = []
	num_cell = 0
	f = open(expressionFile)
	tx = {}
	for l in f:
		l = l.rstrip("\n")
		ll = l.split("\t")
		g = ll[0]
		val = [float(v) for v in ll[1:]]
		num_cell = len(val)
		tx[g] = val
		genes.append(g)
	f.close()
	X = np.empty((num_cell, len(genes)), dtype="float32")
	for ig, g in enumerate(genes):
		for i in range(num_cell):
			X[i, ig] = tx[g][i]
	labels = []
	labels_2 = []
	f = open(labelFile)
	for l in f:
		l = l.rstrip("\n")
		ll = l.split("\t")
		labels.append(ll[0])
		labels_2.append(ll[1])
	f.close()
	return X, genes, labels, labels_2

def read_tasic_X(expressionFile, labelFile):
	genes = []
	num_cell = 0
	f = open(expressionFile)
	tx = {}
	for l in f:
		l = l.rstrip("\n")
		ll = l.split("\t")
		g = ll[0]
		val = [float(v) for v in ll[1:]]
		num_cell = len(val)
		tx[g] = val
		genes.append(g)
	f.close()
	X = np.empty((num_cell, len(genes)), dtype="float32")
	for ig, g in enumerate(genes):
		for i in range(num_cell):
			X[i, ig] = tx[g][i]
	labels = []
	label_id = []
	labels_2 = []
	f = open(labelFile)
	for l in f:
		l = l.rstrip("\n")
		ll = l.split("\t")
		labels.append(ll[0])
		label_id.append(ll[1])
		labels_2.append(ll[2])
	f.close()
	return X, genes, labels, labels_2, label_id

def read_zeisel_X(expressionFile, labelFile):
	genes = []
	num_cell = 0
	f = open(expressionFile)
	tx = {}
	for l in f:
		l = l.rstrip("\n")
		ll = l.split("\t")
		g = ll[0]
		val = [float(v) for v in ll[1:]]
		num_cell = len(val)
		tx[g] = val
		genes.append(g)
	f.close()
	X = np.empty((num_cell, len(genes)), dtype="float32")
	for ig, g in enumerate(genes):
		for i in range(num_cell):
			X[i, ig] = tx[g][i]
	labels = []
	label_id = []
	f = open(labelFile)
	for l in f:
		l = l.rstrip("\n")
		ll = l.split("\t")
		labels.append(ll[0])
		label_id.append(ll[1])
	f.close()
	return X, genes, labels, label_id

def read_habib_X(expressionFile, labelFile):
	genes = []
	num_cell = 0
	f = open(expressionFile)
	tx = {}
	for l in f:
		l = l.rstrip("\n")
		ll = l.split("\t")
		g = ll[0]
		val = [float(v) for v in ll[1:]]
		num_cell = len(val)
		tx[g] = val
		genes.append(g)
	f.close()
	X = np.empty((num_cell, len(genes)), dtype="float32")
	for ig, g in enumerate(genes):
		for i in range(num_cell):
			X[i, ig] = tx[g][i]
	labels = []
	label_id = []
	f = open(labelFile)
	for l in f:
		l = l.rstrip("\n")
		ll = l.split("\t")
		labels.append(ll[0])
		label_id.append(ll[1])
	f.close()
	return X, genes, labels, label_id


def get_ID(l, genes):
	sl = set(l)
	list_i = []
	for i, v in enumerate(genes):
		if v in sl:
			list_i.append(i)
	return np.array(list_i)

